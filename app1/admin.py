from django.contrib import admin

# Register your models here.
from .models import Setting_value_0,Setting_value_1,Setting_value_2,Setting_value_3,Setting_value_4,Setting_value_5,Setting_value_Ex1

admin.site.register(Setting_value_0)
admin.site.register(Setting_value_1)
admin.site.register(Setting_value_2)
admin.site.register(Setting_value_3)
admin.site.register(Setting_value_4)
admin.site.register(Setting_value_5)
admin.site.register(Setting_value_Ex1)
