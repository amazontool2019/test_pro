from django.db import models
# from accounts.models import CustomUser

# Create your models here.
class Setting_value_0(models.Model):
    UserID = models.CharField(max_length=30)
    cmp_price = models.IntegerField(default=200)
    bid = models.IntegerField(default=16)
    cmp_sep = models.CharField(default='_',max_length=1)
    def_keyword = models.CharField(max_length=30,blank=True)

    def __str__(self):
        return self.UserID

class Setting_value_1(models.Model):
    UserID = models.CharField(max_length=30)
    click_through = models.FloatField(default=0.003)

    def __str__(self):
        return self.UserID

class Setting_value_2(models.Model):
    UserID = models.CharField(max_length=30)
    click_through = models.FloatField(default=0.003)
    click_add = models.IntegerField(default=3)

    def __str__(self):
        return self.UserID

class Setting_value_3(models.Model):
    UserID = models.CharField(max_length=30)
    impression1 = models.IntegerField(default=1000)
    impression2 = models.IntegerField(default=500)
    click_through1 = models.FloatField(default=0.005)
    click_through2 = models.FloatField(default=0.003)
    click_through3 = models.FloatField(default=0.002)
    click_through4 = models.FloatField(default=0.001)

    def __str__(self):
        return self.UserID


class Setting_value_4(models.Model):
    UserID = models.CharField(max_length=30)
    click_through = models.FloatField(default=0.003)
    impression = models.IntegerField(default=500)
    conversion = models.FloatField(default=0.02)

    def __str__(self):
        return self.UserID

class Setting_value_5(models.Model):
    UserID = models.CharField(max_length=30)
    click_through = models.FloatField(default=0.005)
    impression = models.IntegerField(default=2000)
    conversion = models.FloatField(default=0.02)
    bid_add = models.IntegerField(default=3)

    def __str__(self):
        return self.UserID

class Setting_value_Ex1(models.Model):
    UserID = models.CharField(max_length=30)
    sales_lower_rate = models.FloatField(default=0.5)
    impression = models.IntegerField(default=500)
    acos = models.FloatField(default=0.5)

    def __str__(self):
        return self.UserID
