from django.http import HttpResponse
from .models import Setting_value_0,Setting_value_1,Setting_value_2,Setting_value_3,Setting_value_4,Setting_value_5,Setting_value_Ex1
from django.shortcuts import render ,redirect
from .forms import UploadForm_0,UploadForm_1,UploadForm_2,UploadForm_3,UploadForm_4,UploadForm_5,UploadForm_Ex1_1,UploadForm_Ex1_2
import pandas as pd
import numpy as np
import math
import shutil #ファイルコピー

import openpyxl #execl操作

import os

import urllib
import datetime

from datetime import datetime as dt

from django.http import FileResponse
from django.contrib.auth.decorators import login_required

@login_required
def upload0(request):

    print('upload0')

    user_name = request.user

    complete_mes=''

    if Setting_value_0.objects.filter(UserID=user_name).exists():
        obj = Setting_value_0.objects.get(UserID=user_name)
    else:
        obj = Setting_value_0()
        obj.UserID = user_name


    if (request.method == 'POST'):

        print('upload0:POST')
        # uf = UploadForm_0(request.POST, request.FILES)
        # uf = UploadForm(request.POST,instance=obj)
        uf = UploadForm_0(request.POST,request.FILES,instance=obj)

        if uf.is_valid():


            print('upload0:is_valid')
            MEDIA_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/media/'
            STATIC_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/static/'

            # アップロードファイル名を取得
            upload_filename = uf.save()

            #アップロードファイルの読込
            up_df= pd.read_excel(MEDIA_URL + upload_filename, index_col=None,dtype={'親ASIN':'object','子SKU':'object','商品タイトル':'object'})

            #エクセルの読み込み
            try:

                data_cnt=up_df['子SKU'].count()

                #欠損データの補完
                # up_df['親ASIN']=up_df['親ASIN'].fillna(method='ffill')
                #親ASINがなければNanを空にする。
                up_df['親ASIN']=up_df['親ASIN'].fillna('')
                up_df['商品タイトル']=up_df['商品タイトル'].fillna('')

                #不要データの削除
                up_df.dropna()

                #すべて文字列型にする。
                up_df['親ASIN']=up_df['親ASIN'].astype(str)
                up_df['子SKU']=up_df['子SKU'].astype(str)
                up_df['商品タイトル']=up_df['商品タイトル'].astype(str)


                temp_asin =''

                # テンプレートファイルの取得
                # temp_df= pd.read_excel(MEDIA_URL + 'temp_1.xlsx', index_col=None)
                temp_df= pd.read_excel(STATIC_URL + 'temp_1.xlsx', index_col=None)

                print('read ok')
                now = datetime.datetime.now()
                date_str = (now.strftime("%Y/%m/%d"))

                for i in range(0, data_cnt):
                    #親ASINが新規の場合
                    # if  up_df.iloc[i, up_df.columns.get_loc('親ASIN')] != temp_asin :
                    # if  up_df.iloc[i, up_df.columns.get_loc('親ASIN')] != 'test' :

                    temp_asin =up_df.iloc[i, up_df.columns.get_loc('親ASIN')]

                    print('temp_asin='+ str(temp_asin))

                    #１行目追加
                    temp_df = temp_df.append({
                    'キャンペーン' : 'A' + obj.cmp_sep + temp_asin + " " + up_df.iloc[i, up_df.columns.get_loc('商品タイトル')],
                    'キャンペーンの1日の予算' : obj.cmp_price,
                    'キャンペーン開始日' : date_str,
                    'キャンペーンターゲティングタイプ' : 'auto',
                    'キャンペーンステータス' : '有効',
                    '入札戦略' : '動的な入札（ダウンのみ)'
                    } , ignore_index=True)

                    print("clear")
                    #2行目追加
                    temp_df = temp_df.append({
                    'キャンペーン' : 'A' + obj.cmp_sep + temp_asin + " " +up_df.iloc[i, up_df.columns.get_loc('商品タイトル')],
                    '広告グループ' : '広告グループ 1',
                    '入札額上限' : obj.bid,
                    '広告グループステータス' : '有効',
                    'キャンペーンステータス' : '有効'
                    } , ignore_index=True)


                    #１行目追加
                    #3行目追加
                    temp_df=temp_df.append({
                    'キャンペーン' : 'A' + obj.cmp_sep + temp_asin + " " +up_df.iloc[i, up_df.columns.get_loc('商品タイトル')],
                    '広告グループ' : '広告グループ 1',
                    'SKU' : up_df.iloc[i, up_df.columns.get_loc('子SKU')],
                    '広告グループステータス' : '有効',
                    'キャンペーンステータス' : '有効',
                    'ステータス' : '有効'
                    } , ignore_index=True)

                    #１行目追加
                    #キーワードがあった場合（オートはコピー後削除)

                    #match_typeの追加
                    match_type = ""

                    if obj.def_keyword != "":
                        match_type = 'Phrase'
                        print(match_type)

                    temp_df = temp_df.append({
                    'キャンペーン' : 'A' + obj.cmp_sep + temp_asin + " " +up_df.iloc[i, up_df.columns.get_loc('商品タイトル')],
                    '広告グループ' : '広告グループ 1',
                    '入札額上限' : obj.bid,
                    '広告グループステータス' : '有効',
                    'キャンペーンステータス' : '有効',
                    'ステータス' : '有効',
                    'キーワードまたは商品ターゲティング':obj.def_keyword,
                    'マッチタイプ' : match_type
                    } , ignore_index=True)

                #デバッグ用出力
                # temp_df.to_excel(MEDIA_URL + 'debug_0.xlsx' , index=False, sheet_name='sheet1')

                #コピー
                temp_df2 = temp_df.copy()

                #A→P
                temp_df2['キャンペーン']='P' + temp_df2['キャンペーン'].str[1:]

                #キャンペーンターゲティングタイプの置換
                temp_df2['キャンペーンターゲティングタイプ'] = temp_df2.キャンペーンターゲティングタイプ.replace('auto','manual')
                #コピー
                temp_df3 = temp_df2.copy()

                #P→E
                temp_df3['キャンペーン']='E' + temp_df3['キャンペーン'].str[1:]

                #2020/12/9 仕様変更マッチタイプの追加
                rep_match_temp = temp_df3["マッチタイプ"].replace("Phrase", "Exact", regex=True)
                temp_df3['マッチタイプ']=rep_match_temp

                #データフレームのデータ結合
                temp_df = pd.concat([temp_df, temp_df2])

                temp_df = pd.concat([temp_df, temp_df3])

                #オートのキーワードまたは商品ターゲティングを除外
                temp_df=temp_df[~((temp_df['ステータス']=='有効') &( temp_df['キャンペーン'].str.startswith('A'))&(~temp_df['キーワードまたは商品ターゲティング'].isnull()))]


                dt_now = datetime.datetime.now()
                d_str = dt_now.strftime('%Y%m%d%H%M%S')

                #出力ファイル名
                output_filename = '0_新規広告作成' + d_str +'.xlsx'
                # output_filename = '0_output' + '.xlsx'

                temp_df.to_excel(MEDIA_URL + output_filename , index=False, sheet_name='sheet1')

                wb = openpyxl.load_workbook(MEDIA_URL + output_filename)
                response = HttpResponse(content_type='application/vnd.ms-excel')
                # response['Content-Disposition'] = 'attachment; filename=' + output_filename
                response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(fn=urllib.parse.quote(output_filename))

                wb.save(response)
                return response

            except :
                return redirect('/upload_error')
                print("エラー")
            else:
                print('例外は発生しませんでした。')


    if (request.GET.get('param1')==1):
        complete_mes="test"


    context = {

        'form':UploadForm_0(instance=obj),
        'Setting': Setting_value_0.objects.filter(UserID=user_name),
        'page':0,
        'complete_mes':complete_mes,
        'title':'Ex0_新規広告作成',
        'user_ID':user_name,
        'help_text':'新規広告作成用のファイルを選択して下さい。',
    }

    return render(request , 'UploadEx0.html',context)

@login_required
def upload1(request):

    print('upload1')

    user_name = request.user

    if Setting_value_1.objects.filter(UserID=user_name).exists():

        obj = Setting_value_1.objects.get(UserID=user_name)
    else:
        obj = Setting_value_1()
        obj.UserID = user_name


    if (request.method == 'POST'):

        uf = UploadForm_1(request.POST,request.FILES,instance=obj)
        # uf = UploadForm_1(request.POST, request.FILES)
        # uf = UploadForm(request.POST,instance=obj)

        if uf.is_valid():


            MEDIA_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/media/'
            STATIC_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/static/'

            # アップロードファイル名を取得
            upload_filename = uf.save()

            #アップロードファイルの読込
            up_df= pd.read_excel(MEDIA_URL + upload_filename, index_col=None)


            try:

    # データ絞り込み
                #2021/7 対象データ以外の除外
                up_df =up_df[
                                (
                                (up_df['キャンペーン名'].str.startswith('A' )) |
                                (up_df['キャンペーン名'].str.startswith('P' )) |
                                (up_df['キャンペーン名'].str.startswith('E' ))
                                )
                ]

                up_filter = up_df[
                (up_df['広告がクリックされてから7日間の総売上高']==0 )&
                ~(up_df['カスタマーの検索キーワード'].str.startswith('b0')) &
                # (up_df['クリックスルー率 (CTR)']<0.003) &
                (up_df['クリックスルー率 (CTR)']< obj.click_through) &
                (up_df['ターゲティング']=='*')
                ]
                #デバッグ用出力
                # up_filter.to_excel(MEDIA_URL + 'debug.xlsx' , index=False, sheet_name='sheet1')

                # テンプレートファイルの取得
                # temp_df= pd.read_excel(MEDIA_URL + 'temp_1.xlsx', index_col=None)
                temp_df= pd.read_excel(STATIC_URL + 'temp_1.xlsx', index_col=None)

                # 抽出データのセット

                temp_df['キャンペーン'] = up_filter['キャンペーン名']
                temp_df['広告グループ'] = up_filter['広告グループ名']
                temp_df['キーワードまたは商品ターゲティング'] = up_filter['カスタマーの検索キーワード']

                # 初期データセット
                temp_df['マッチタイプ'] = 'Negative Exact'
                temp_df['ステータス'] = '有効'

                # temp_df.to_excel(MEDIA_URL + 'test.xlsx', index=False, sheet_name='sheet1')

                #出力ファイル名

                dt_now = datetime.datetime.now()
                d_str = dt_now.strftime('%Y%m%d%H%M%S')

                #出力ファイル名
                output_filename = '1_オート広告除外' + d_str +'.xlsx'

                temp_df.to_excel(MEDIA_URL + output_filename , index=False, sheet_name='sheet1')

                wb = openpyxl.load_workbook(MEDIA_URL + output_filename)
                response = HttpResponse(content_type='application/vnd.ms-excel')
                # response['Content-Disposition'] = 'attachment; filename=' + output_filename
                response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(fn=urllib.parse.quote(output_filename))
                wb.save(response)
                return response


            except :
                return redirect('/upload_error')
                print("エラー")
            else:
                print('例外は発生しませんでした。')

    else:
        obj.click_through = obj.click_through*100

    context = {

        'form':UploadForm_1(instance=obj),
        'Setting': Setting_value_1.objects.filter(UserID=user_name),
        'page':1,
        'title':'1_オート広告除外',
        'user_ID':user_name,
        'help_text':'「スポンサープロダクト広告 検索ワード レポート60日分」ファイル選択して下さい',

    }

    return render(request , 'Upload1.html',context)


@login_required
def upload2(request):

    print('upload2')

    user_name = request.user

    if Setting_value_2.objects.filter(UserID=user_name).exists():
        obj = Setting_value_2.objects.get(UserID=user_name)
    else:
        obj = Setting_value_2()
        obj.UserID = user_name

    if (request.method == 'POST'):

        # uf = UploadForm_2(request.POST, request.FILES)
        # uf = UploadForm(request.POST,instance=obj)
        uf = UploadForm_2(request.POST,request.FILES,instance=obj)

        if uf.is_valid():


            MEDIA_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/media/'
            STATIC_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/static/'

            # アップロードファイル名を取得
            upload_filename = uf.save()

            #アップロードファイルの読込
            up_df= pd.read_excel(MEDIA_URL + upload_filename, index_col=None)


            dt_now = datetime.datetime.now()
            print("ファイル読み込み" + dt_now.strftime('%H:%M:%S'))

            try:
                # データ絞り込み
                #共通

                #2021/7 対象データ以外の除外
                up_df =up_df[
                                (
                                (up_df['キャンペーン名'].str.startswith('A' )) |
                                (up_df['キャンペーン名'].str.startswith('P' )) |
                                (up_df['キャンペーン名'].str.startswith('E' ))
                                )
                ]


                up_filter1 = up_df[
                ~(up_df['カスタマーの検索キーワード'].str.startswith('b0')) &
                ~(up_df['クリック']==0)&
                ((up_df['キャンペーン名'].str.startswith('A' )) | (up_df['キャンペーン名'].str.startswith('P' )))
                ]

                #filter2をコピー
                up_filter2 = up_filter1.copy()

                #up_filter1
                up_filter1 = up_filter1[
                # (up_filter1['クリックスルー率 (CTR)']<0.003) &
                (up_filter1['クリックスルー率 (CTR)']<obj.click_through) &
                ~(up_filter1['広告がクリックされてから7日間の総売上高']==0)
                ]
                #確認ファイル
                # up_filter1.to_excel(MEDIA_URL + 'up_filter21.xlsx' , index=False, sheet_name='sheet1')

                up_filter1['キャンペーン名']='P' + up_filter1['キャンペーン名'].str[1:]


                up_filter2 = up_filter2[
                # (up_filter2['クリックスルー率 (CTR)']>0.003)
                (up_filter2['クリックスルー率 (CTR)']>obj.click_through)
                ]

                up_filter2['キャンペーン名']='P' + up_filter2['キャンペーン名'].str[1:]

                # up_filter2.to_excel(MEDIA_URL + 'up_filter22.xlsx' , index=False, sheet_name='sheet1')


                #データフレームのデータ結合
                summary_df = pd.concat([up_filter1, up_filter2])

                # summary_df.to_excel(MEDIA_URL + 'up_filter2sum.xlsx' , index=False, sheet_name='sheet1')
                #重複削除処理☆☆☆
                #キャンペーン名とターゲティングで重複チェック 削除
                # summary_df.drop_duplicates(['キャンペーン名','カスタマーの検索キーワード'], keep=False, inplace=True)

                dfn = summary_df.copy()
                for _, group in summary_df.groupby('キャンペーン名'):
                    if len(group) <= 1:
                        continue
                    current = group.ターゲティング.tolist()
                    for row in group.itertuples():
                        if row.カスタマーの検索キーワード in current:
                            dfn = dfn.drop(row.Index, axis=0)



                # dfn.to_excel(MEDIA_URL + 'up_filter2削除.xlsx' , index=False, sheet_name='sheet1')

                summary_df = dfn
                #
                # #ターゲティングとカスタマーの検索キーワードが一致しない。
                summary_df =summary_df[summary_df['ターゲティング'] != summary_df['カスタマーの検索キーワード']]
                #
                # summary_df.to_excel(MEDIA_URL + 'up_filter2一致していないもの.xlsx' , index=False, sheet_name='sheet1')

                # テンプレートファイルの取得
                # temp_df= pd.read_excel(MEDIA_URL + 'temp_1.xlsx', index_col=None)
                temp_df= pd.read_excel(STATIC_URL + 'temp_1.xlsx', index_col=None)

                #データ
                temp_df['キャンペーン'] = summary_df['キャンペーン名']
                temp_df['広告グループ'] = summary_df['広告グループ名']
                temp_df['キーワードまたは商品ターゲティング'] = summary_df['カスタマーの検索キーワード']
                # temp_df['入札額上限'] = summary_df['平均クリック単価 (CPC)']+ 3 #☆☆☆　設定シートのデータ
                temp_df['入札額上限'] = summary_df['平均クリック単価 (CPC)']+ obj.click_add
                temp_df['マッチタイプ'] = 'Phrase'
                temp_df['ステータス'] = '有効'

                #入札額上限が2未満の場合は2を設定する。
                temp_df.loc[temp_df["入札額上限"]<2, '入札額上限'] = 2

                #入札額上限を整数で四捨五入する。
                temp_df['入札額上限'] = temp_df['入札額上限'].round(0).astype(np.int64)


                #出力ファイル名
                dt_now = datetime.datetime.now()
                d_str = dt_now.strftime('%Y%m%d%H%M%S')

                #出力ファイル名
                output_filename = '2_キーワード仕入れ' + d_str +'.xlsx'

                temp_df.to_excel(MEDIA_URL + output_filename , index=False, sheet_name='sheet1')

                dt_now = datetime.datetime.now()
                print("ファイル作成" + dt_now.strftime('%H:%M:%S'))

                wb = openpyxl.load_workbook(MEDIA_URL + output_filename)
                response = HttpResponse(content_type='application/vnd.ms-excel')
                # response['Content-Disposition'] = 'attachment; filename=' + output_filename
                response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(fn=urllib.parse.quote(output_filename))
                wb.save(response)
                return response


            except :
                return redirect('/upload_error')
                print("エラー")
            else:
                print('例外は発生しませんでした。')

    else:
        obj.click_through = obj.click_through*100

    context = {

        'form':UploadForm_2(instance=obj),
        'Setting': Setting_value_2.objects.filter(UserID=user_name),
        'page':2,
        'title':'2_キーワード仕入れ',
        'user_ID':user_name,
        'help_text':'「スポンサープロダクト広告 検索ワード レポート60日分」ファイル選択して下さい。',


    }

    return render(request , 'Upload2.html',context)



@login_required
def upload3(request):

    print('upload3')

    user_name = request.user


    if Setting_value_3.objects.filter(UserID=user_name).exists():
        obj = Setting_value_3.objects.get(UserID=user_name)
    else:
        obj = Setting_value_3()
        obj.UserID = user_name


    if (request.method == 'POST'):

        # uf = UploadForm_3(request.POST, request.FILES)
        # uf = UploadForm(request.POST,instance=obj)
        uf = UploadForm_3(request.POST,request.FILES,instance=obj)

        if uf.is_valid():


            MEDIA_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/media/'
            STATIC_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/static/'

            # アップロードファイル名を取得
            upload_filename = uf.save()

            try:
                #アップロードファイルの読込
                up_df= pd.read_excel(MEDIA_URL + upload_filename,sheet_name='スポンサープロダクト広告キャンペーン', index_col=None)




                #2021/7 対象データ以外の除外
                up_df =up_df[
                                (
                                (up_df['キャンペーン'].str.startswith('A' )) |
                                (up_df['キャンペーン'].str.startswith('P' )) |
                                (up_df['キャンペーン'].str.startswith('E' ))
                                )
                ]


                # データ絞り込み
                up_filter = up_df[
                ((up_df['レコードタイプ']=='広告グループ' ) | (up_df['レコードタイプ']=='キーワード' ))&
                ~(up_df['クリック']==0)&
                ~(up_df['キーワードまたは商品ターゲティング']=='*')

                ]

                # テンプレートファイルの取得
                # temp_df= pd.read_excel(MEDIA_URL + 'temp_1.xlsx', index_col=None)
                temp_df= pd.read_excel(STATIC_URL + 'temp_1.xlsx', index_col=None)

                up_filter = up_filter.assign(クリックスルー率=up_df['クリック']/up_df['インプレッション'])

                #平均クリック単価の設定
                # up_filter['入札額上限'] = up_filter['広告費'] / up_filter['クリック']


                # temp_df['キーワードまたは商品ターゲティング'] = up_filter['キーワードまたは商品ターゲティング']

                # temp_df['マッチタイプ'] = up_filter['マッチタイプ']

                # temp_df['広告グループステータス'] = up_filter['広告グループステータス']

                # temp_df['ステータス'] =  up_filter['ステータス']


                # up_filter.to_excel(MEDIA_URL + 'debug3-1.xlsx' , index=False, sheet_name='sheet1')

                #キャンペーン名が'A'から始まる場合はの入札額上限に加算する（☆設定シートから読込)
                up_filter.loc[up_filter["キャンペーン"].str.startswith("A"), '入札額上限'] = up_filter['広告費'] / up_filter['クリック'] + 3
                # up_filter.loc[up_filter["キャンペーン"].str.startswith("A"), '入札戦略'] = 'キャンペーンA'

                #インプレッション 1000以上
                #Type A
                up_filter.loc[~(up_filter["キャンペーン"].str.startswith("A")) &
                # (up_filter['インプレッション']>=1000) &
                (up_filter['インプレッション']>=obj.impression1) &
                # (up_filter['クリックスルー率']>=0.005)
                (up_filter['クリックスルー率']>=obj.click_through1)
                # , '入札額上限'] += 3
                , '入札額上限'] = up_filter['広告費'] / up_filter['クリック'] +3
                # , '入札戦略'] = 'A'


                #Type B
                up_filter.loc[~(up_filter["キャンペーン"].str.startswith("A")) &
                # (up_filter['インプレッション']>=1000)&
                (up_filter['インプレッション']>=obj.impression1) &
                # (up_filter['クリックスルー率']>=0.003) &
                (up_filter['クリックスルー率']>=obj.click_through2) &
                # (up_filter['クリックスルー率']< 0.005)
                (up_filter['クリックスルー率']< obj.click_through1)
                # , '入札額上限'] += 3
                , '入札額上限'] = up_filter['広告費'] / up_filter['クリック'] +3
                # , '入札戦略'] = 'B'


                #Type C
                up_filter.loc[~(up_filter["キャンペーン"].str.startswith("A")) &
                # (up_filter['インプレッション']>=1000)&
                (up_filter['インプレッション']>=obj.impression1) &
                # (up_filter['クリックスルー率']<0.003) &
                (up_filter['クリックスルー率']<obj.click_through2) &
                # (up_filter['クリックスルー率']>= 0.002)
                (up_filter['クリックスルー率']>=obj.click_through3)
                , '入札額上限'] = up_filter['広告費'] / up_filter['クリック'] +2
                # , '入札額上限'] += 2
                # , '入札戦略'] = 'C'


                #Type D
                up_filter.loc[~(up_filter["キャンペーン"].str.startswith("A")) &
                # (up_filter['インプレッション']>=1000) &
                (up_filter['インプレッション']>=obj.impression1) &
                # (up_filter['クリックスルー率']<0.002)
                (up_filter['クリックスルー率']<obj.click_through3)&
                # (up_filter['クリックスルー率']>=0.001)
                (up_filter['クリックスルー率']>=obj.click_through4)
                , '入札額上限'] = up_filter['広告費'] / up_filter['クリック'] -2
                # , '入札額上限'] -= 2
                # , '入札戦略'] = 'D'


                # インプレッション 1000未満 500以上
                #Type E
                up_filter.loc[~(up_filter["キャンペーン"].str.startswith("A")) &
                # (up_filter['インプレッション']>=1000) &
                (up_filter['インプレッション']>=obj.impression1) &
                # (up_filter['クリックスルー率']<0.001)
                (up_filter['クリックスルー率']<obj.click_through4)
                , '入札額上限'] = 3
                # , '入札戦略'] = 'E'


                #Type A' 1000>インプレッション>=500
                up_filter.loc[~(up_filter["キャンペーン"].str.startswith("A")) &
                # (up_filter['インプレッション']<500) &
                (up_filter['インプレッション']<obj.impression1) &
                # (up_filter['インプレッション']>=500) &
                (up_filter['インプレッション']>=obj.impression2) &
                # (up_filter['クリックスルー率']>=0.005)
                (up_filter['クリックスルー率']>=obj.click_through1)
                # , '入札額上限'] += 3
                , '入札額上限'] = up_filter['広告費'] / up_filter['クリック'] +3
                # , '入札戦略'] = 'A/''


                #Type B' 1000>インプレッション>=500
                up_filter.loc[~(up_filter["キャンペーン"].str.startswith("A")) &
                # (up_filter['インプレッション']<5000) &
                (up_filter['インプレッション']<obj.impression1) &
                # (up_filter['インプレッション']>=500) &
                (up_filter['インプレッション']>=obj.impression2) &
                # (up_filter['クリックスルー率']< 0.005)
                (up_filter['クリックスルー率']<obj.click_through1) &
                # (up_filter['クリックスルー率'] 0.003)
                (up_filter['クリックスルー率']>= obj.click_through2)
                # , '入札額上限'] += 3
                , '入札額上限'] = up_filter['広告費'] / up_filter['クリック'] +3
                # , '入札戦略'] = 'B/''



                #インプレッション 500未満
                #Type F
                up_filter.loc[~(up_filter["キャンペーン"].str.startswith("A")) &
                # (up_filter['インプレッション']< 500) &
                (up_filter['インプレッション']< obj.impression2) &
                # (up_filter['クリックスルー率']>=0.005)
                (up_filter['クリックスルー率']>= obj.click_through1)
                # ,'入札額上限'] += 6
                , '入札額上限'] = up_filter['広告費'] / up_filter['クリック'] +6
                # , '入札戦略'] = 'F'

                up_filter['入札額上限'].fillna(9999, inplace=True)

                #平均クリック単価が2未満の場合は2を設定する。
                up_filter.loc[up_filter["入札額上限"]<2, '入札額上限'] = 2

                #入札額上限を整数で四捨五入する。
                up_filter['入札額上限'] = up_filter['入札額上限'].round(0).astype(np.int64)

                up_filter.loc[up_filter["入札額上限"]==9999, '入札額上限'] = np.nan

                #Debug用
                # up_filter.to_excel(MEDIA_URL + 'debug3-2.xlsx' , index=False, sheet_name='sheet1')

                temp_df['入札額上限'] = up_filter['入札額上限']

                temp_df['キャンペーン'] = up_filter['キャンペーン']

                temp_df['広告グループ'] = up_filter['広告グループ']


                temp_df['キーワードまたは商品ターゲティング'] = up_filter['キーワードまたは商品ターゲティング']

                temp_df['マッチタイプ'] = up_filter['マッチタイプ']

                temp_df['広告グループステータス'] = up_filter['広告グループステータス']

                temp_df['ステータス'] =  up_filter['ステータス']


                #マッチタイプ置き換え　不要になったら削除
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('フレーズ','Phrase')
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('完全一致','Exact')
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('除外完全一致','Negative Exact')

                #出力ファイル名
                dt_now = datetime.datetime.now()
                d_str = dt_now.strftime('%Y%m%d%H%M%S')

                #出力ファイル名
                output_filename = '3_入札額調整' + d_str +'.xlsx'

                temp_df.to_excel(MEDIA_URL + output_filename , index=False, sheet_name='sheet1')

                wb = openpyxl.load_workbook(MEDIA_URL + output_filename)
                response = HttpResponse(content_type='application/vnd.ms-excel')
                # response['Content-Disposition'] = 'attachment; filename=' + output_filename
                response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(fn=urllib.parse.quote(output_filename))
                wb.save(response)
                return response


            except :

                print("エラー")
                return redirect('/upload_error')
            else:
                print('例外は発生しませんでした。')

    else:
        obj.click_through1 = obj.click_through1*100
        obj.click_through2 = obj.click_through2*100
        obj.click_through3 = obj.click_through3*100
        obj.click_through4 = obj.click_through4*100


    context = {

        'form':UploadForm_3(instance=obj),
        'Setting': Setting_value_3.objects.filter(UserID=user_name),
        'page':3,
        'title':'3_入札額調整',
        'user_ID':user_name,
        'help_text':'「バルク操作ファイル７日分 」ファイル選択して下さい。',


    }

    return render(request , 'Upload3.html',context)



@login_required
def upload4(request):

    print('upload4')

    user_name = request.user

    if Setting_value_4.objects.filter(UserID=user_name).exists():
        obj = Setting_value_4.objects.get(UserID=user_name)
    else:
        obj = Setting_value_4()
        obj.UserID = user_name


    if (request.method == 'POST'):

        # uf = UploadForm_4(request.POST, request.FILES)
        # uf = UploadForm(request.POST,instance=obj)
        uf = UploadForm_4(request.POST,request.FILES,instance=obj)

        if uf.is_valid():


            MEDIA_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/media/'
            STATIC_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/static/'

            # アップロードファイル名を取得
            upload_filename = uf.save()

            try:
                #アップロードファイルの読込
                up_df= pd.read_excel(MEDIA_URL + upload_filename,sheet_name='スポンサープロダクト広告キャンペーン', index_col=None)

                #2021/7 対象データ以外の除外
                up_df =up_df[
                                (
                                (up_df['キャンペーン'].str.startswith('A' )) |
                                (up_df['キャンペーン'].str.startswith('P' )) |
                                (up_df['キャンペーン'].str.startswith('E' ))
                                )
                ]

                #クリックスルー率、コンバージョン率を追加する。
                up_df = up_df.assign(クリックスルー率=up_df['クリック']/up_df['インプレッション'])
                up_df = up_df.assign(コンバージョン率=up_df['注文数']/up_df['クリック'])


                # データ絞り込み
                up_filter1 = up_df[
                (up_df['レコードタイプ']=='キーワード' )&
                (up_df['キャンペーン'].str.startswith('P' ))&
                ~(up_df['クリック']==0)&
                # (up_df['クリックスルー率']<0.003)
                (up_df['クリックスルー率']<obj.click_through)&
                #仕様追加
                ~(up_df['キーワードまたは商品ターゲティング']=='*')

                ]


                # up_filter1.to_excel(MEDIA_URL + 'up_filter41.xlsx' , index=False, sheet_name='sheet1')

                up_filter2 = up_df[
                (up_df['レコードタイプ']=='キーワード' )&
                (up_df['キャンペーン'].str.startswith('P' ))&
                ~(up_df['クリック']==0)&
                # (up_df['インプレッション']>= 500 )&
                # (up_df['コンバージョン率']<0.02)
                (up_df['インプレッション']>= obj.impression )&
                (up_df['コンバージョン率']< obj.conversion)&
                ~(up_df['キーワードまたは商品ターゲティング']=='*')

                ]


                # up_filter2.to_excel(MEDIA_URL + 'up_filter42.xlsx' , index=False, sheet_name='sheet1')

                #データフレームのデータ結合
                summary_df = pd.concat([up_filter1, up_filter2])



                # テンプレートファイルの取得
                # temp_df= pd.read_excel(MEDIA_URL + 'temp_1.xlsx', index_col=None)
                temp_df= pd.read_excel(STATIC_URL + 'temp_1.xlsx', index_col=None)

                #データ
                temp_df['キャンペーン'] = summary_df['キャンペーン']
                temp_df['広告グループ'] = summary_df['広告グループ']
                temp_df['入札額上限'] = summary_df['入札額上限']


                temp_df['キーワードまたは商品ターゲティング'] = summary_df['キーワードまたは商品ターゲティング']

                temp_df['マッチタイプ'] = 'Phrase'

                temp_df['ステータス'] =  '一時停止'

                #マッチタイプ置き換え　不要になったら削除
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('フレーズ','Phrase')
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('完全一致','Exact')
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('除外完全一致','Negative Exact')


                #出力ファイル名
                dt_now = datetime.datetime.now()
                d_str = dt_now.strftime('%Y%m%d%H%M%S')

                #出力ファイル名
                output_filename = '4_マニュアル保留設定' + d_str +'.xlsx'

                temp_df.to_excel(MEDIA_URL + output_filename , index=False, sheet_name='sheet1')

                wb = openpyxl.load_workbook(MEDIA_URL + output_filename)
                response = HttpResponse(content_type='application/vnd.ms-excel')
                # response['Content-Disposition'] = 'attachment; filename=' + output_filename
                response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(fn=urllib.parse.quote(output_filename))
                wb.save(response)
                return response


            except :
                return redirect('/upload_error')
                print("エラー")
            else:
                print('例外は発生しませんでした。')

    else:
        obj.click_through = obj.click_through*100
        obj.conversion = obj.conversion*100

    context = {

        'form':UploadForm_4(instance=obj),
        'Setting': Setting_value_4.objects.filter(UserID=user_name),
        'page':4,
        'title':'4_マニュアル保留設定',
        'user_ID':user_name,
        'help_text':'「バルク操作ファイル６０日分 」ファイル選択して下さい。',

    }

    return render(request , 'Upload4.html',context)

@login_required
def upload5(request):

    print('upload5')

    user_name = request.user

    if Setting_value_5.objects.filter(UserID=user_name).exists():
        obj = Setting_value_5.objects.get(UserID=user_name)
    else:
        obj = Setting_value_5()
        obj.UserID = user_name


    if (request.method == 'POST'):

        # uf = UploadForm_5(request.POST, request.FILES)
        # uf = UploadForm(request.POST,instance=obj)
        uf = UploadForm_5(request.POST,request.FILES,instance=obj)

        if uf.is_valid():


            MEDIA_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/media/'
            STATIC_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/static/'

            # アップロードファイル名を取得
            upload_filename = uf.save()

            try:

                #アップロードファイルの読込
                up_df= pd.read_excel(MEDIA_URL + upload_filename,sheet_name='スポンサープロダクト広告キャンペーン', index_col=None)

                #2021/7 対象データ以外の除外
                up_df =up_df[
                                (
                                (up_df['キャンペーン'].str.startswith('A' )) |
                                (up_df['キャンペーン'].str.startswith('P' )) |
                                (up_df['キャンペーン'].str.startswith('E' ))
                                )
                ]

                #クリックスルー率、コンバージョン率を追加する。
                up_df = up_df.assign(クリックスルー率=up_df['クリック']/up_df['インプレッション'])
                up_df = up_df.assign(コンバージョン率=up_df['注文数']/up_df['クリック'])


                # データ絞り込み
                up_filter1 = up_df[
                (up_df['レコードタイプ']=='キーワード' )&
                (up_df['キャンペーン'].str.startswith('P' ))&
                ~(up_df['クリック']==0)&
                # (up_df['インプレッション']>= 2000 )&
                # (up_df['クリックスルー率']>=0.005)&
                # (up_df['コンバージョン率']>=0.02)
                (up_df['インプレッション']>= obj.impression )&
                (up_df['クリックスルー率']>= obj.click_through)&
                (up_df['コンバージョン率']>= obj.conversion)&
                ~(up_df['キーワードまたは商品ターゲティング']=='*')
                ]

                up_filter1['マッチタイプ'] = 'Phrase'
                up_filter1['ステータス'] =  '一時停止'

                # up_filter1.to_excel(MEDIA_URL + 'up_filter51.xlsx' , index=False, sheet_name='sheet1')

                up_filter2 = up_filter1.copy()


                up_filter2['キャンペーン']='E' + up_filter2['キャンペーン'].str[1:]
                up_filter2['マッチタイプ']='Exact'
                up_filter2['ステータス']='有効'
                # up_filter2['入札額上限'] +=3
                up_filter2['入札額上限'] +=obj.bid_add

                # up_filter2.to_excel(MEDIA_URL + 'up_filter52.xlsx' , index=False, sheet_name='sheet1')

                #データフレームのデータ結合
                summary_df = pd.concat([up_filter1, up_filter2])

                # summary_df.to_excel(MEDIA_URL + 'up_filter5summ.xlsx' , index=False, sheet_name='sheet1')

                # テンプレートファイルの取得
                # temp_df= pd.read_excel(MEDIA_URL + 'temp_1.xlsx', index_col=None)
                temp_df= pd.read_excel(STATIC_URL + 'temp_1.xlsx', index_col=None)

                #データ
                temp_df['キャンペーン'] = summary_df['キャンペーン']
                temp_df['広告グループ'] = summary_df['広告グループ']
                temp_df['入札額上限'] = summary_df['入札額上限']
                temp_df['マッチタイプ'] = summary_df['マッチタイプ']
                temp_df['ステータス'] = summary_df['ステータス']
                temp_df['キーワードまたは商品ターゲティング'] = summary_df['キーワードまたは商品ターゲティング']

                #入札額上限が2未満の場合は2を設定する。
                temp_df.loc[temp_df["入札額上限"]<2, '入札額上限'] = 2

                #マッチタイプ置き換え　不要になったら削除
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('フレーズ','Phrase')
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('完全一致','Exact')
                temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('除外完全一致','Negative Exact')


                #出力ファイル名
                dt_now = datetime.datetime.now()
                d_str = dt_now.strftime('%Y%m%d%H%M%S')

                #出力ファイル名
                output_filename = '5_完全一致移行' + d_str +'.xlsx'

                temp_df.to_excel(MEDIA_URL + output_filename , index=False, sheet_name='sheet1')

                wb = openpyxl.load_workbook(MEDIA_URL + output_filename)
                response = HttpResponse(content_type='application/vnd.ms-excel')
                # response['Content-Disposition'] = 'attachment; filename=' + output_filename
                response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(fn=urllib.parse.quote(output_filename))
                wb.save(response)
                return response



            except :
                return redirect('/upload_error')
                print("エラー")
            else:
                print('例外は発生しませんでした。')


    else:
        obj.click_through = obj.click_through*100
        obj.conversion = obj.conversion*100

    context = {

        'form':UploadForm_5(instance=obj),
        'Setting': Setting_value_5.objects.filter(UserID=user_name),
        'page':5,
        'title':'5_完全一致移行',
        'user_ID':user_name,
        'help_text':'「バルク操作ファイル６０日分」ファイル選択して下さい。',

    }


    return render(request , 'Upload5.html',context)

def upload_error(request):

    return render(request , 'upload_error.html')




@login_required
def uploadEx1(request):

    print('uploadEx1')

    MEDIA_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/media/'
    STATIC_URL = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/static/'

    user_name = request.user

    if Setting_value_Ex1.objects.filter(UserID=user_name).exists():
        obj = Setting_value_Ex1.objects.get(UserID=user_name)
    else:
        obj = Setting_value_Ex1()
        obj.UserID = user_name

    #％変換
    obj.sales_lower_rate = obj.sales_lower_rate*100
    obj.acos = obj.acos*100

    #postのとき、（初期表示でない場合）
    if (request.method == 'POST'):
        print('valid before')

        #skuがない場合(P1->P２のとき)
        if not(request.POST.get('sku_text',None)):
            print("P2")
            #２P
            uf = UploadForm_Ex1_1(request.POST,request.FILES,instance=obj)

            #入力チェック
            if uf.is_valid():
                print("is_validate:OK")
                # アップロードファイル名を取得
                upload_filename = uf.save()

                try:

                    #csv読み込み
                    df = pd.read_csv(MEDIA_URL + upload_filename,dtype={'SKU':'object'})
                    # df = pd.read_csv(MEDIA_URL + upload_filename,encoding="SHIFT-JIS")


                    # df.to_excel(MEDIA_URL + '置き換え.xlsx' , index=False, sheet_name='sheet1')
                    #文字変換
                    #ダウンロードデータは全角￥だが、Excelで編集すると半角￥になるので注意
                    #2021/7/23 フォーマット修正
                    # df['注文商品売上']=df['注文商品売上'].str.replace('￥','')
                    # df['注文商品売上']=df['注文商品売上'].str.replace('¥','')
                    # df['注文商品売上']=df['注文商品売上'].str.replace(',','')


                    df['売上額']=df['売上額'].str.replace('￥','')
                    df['売上額']=df['売上額'].str.replace('¥','')
                    df['売上額']=df['売上額'].str.replace(',','')


                    # df.to_excel(MEDIA_URL + '置き換え.xlsx' , index=False, sheet_name='sheet1')

                    #型変換
                    #2021/7/23 フォーマット修正
                    # df['注文商品売上']=df['注文商品売上'].astype(np.int64)
                    df['売上額']=df['売上額'].astype(np.int64)

                    #ソートする
                    #2021/7/23 フォーマット修正
                    # df=df.sort_values(by=["注文商品売上"])
                    df=df.sort_values(by=["売上額"])
                    # df_s = df.sort_values('注文商品売上')

                    #下位
                    #SKUデータセット
                    # df_head=df.head(math.ceil(len(df)/2))
                    df_head=df.head(math.ceil(len(df)*obj.sales_lower_rate))

                    sku_data = ','.join(df_head["SKU"])


                    #％変換 validate後でないと値が戻ってしまう。
                    obj.sales_lower_rate = obj.sales_lower_rate*100
                    obj.acos = obj.acos*100

                    #データをSKUに設定
                    context = {
                        'form':UploadForm_Ex1_2(instance=obj),
                        'Setting': Setting_value_Ex1.objects.filter(UserID=user_name),
                        'page':7,
                        'title':'Ex1_広告費削減施策2P',
                        'user_ID':user_name,
                        'help_text':'「バルク操作ファイル６０日分」ファイル選択して下さい。',
                        'SKU':sku_data ,
                    }
                except :
                    return redirect('/upload_error')
                    print("エラー")
                else:
                    print('例外は発生しませんでした。')

            else:
                print('p2 is_valid == False error')
                print("P1")

                context = {
                    'form':UploadForm_Ex1_1(instance=obj),
                    'Setting': Setting_value_Ex1.objects.filter(UserID=user_name),
                    'page':6,
                    'title':'Ex1_広告費削減施策P1',
                    'user_ID':user_name,
                    'help_text':'「ASIN別 詳細ページ売上・トラフィック」ファイルを選択して下さい。',

                }

                #SKUのリストを格納
        #send あり　
        else:
            #P3
            uf = UploadForm_Ex1_2(request.POST,request.FILES,instance=obj)

            if uf.is_valid():

                print("p3")
                # アップロードファイル名を取得
                upload_filename = uf.save()

                print("filename=" + upload_filename)

                try:

                    up_df= pd.read_excel(MEDIA_URL + upload_filename,sheet_name='スポンサープロダクト広告キャンペーン', index_col=None)

                    #2021/7 対象データ以外の除外
                    up_df =up_df[
                                    (
                                    (up_df['キャンペーン'].str.startswith('A' )) |
                                    (up_df['キャンペーン'].str.startswith('P' )) |
                                    (up_df['キャンペーン'].str.startswith('E' ))
                                    )
                    ]

                    #％削除（パーセントがあると文字型認識される）
                    up_df['ACOS'] = up_df['ACOS'].str.replace('%','')

                    # up_df['ACOS'] = up_df['ACOS'].astype(float)

                    # up_df.to_excel(MEDIA_URL + 'パーセント削除.xlsx' , index=False, sheet_name='sheet1')

                    up_filter1 = up_df[
                        (up_df['レコードタイプ']=='広告' ) &
                        # (up_df['インプレッション']>=4000) &
                        (up_df['インプレッション'].astype(float)>=obj.impression) &
                        # (up_df['ACOS'].astype(float)>=0.5)
                        (up_df['ACOS'].astype(float)>=obj.acos*100)
                    ]

                    # up_filter1.to_excel(MEDIA_URL + 'バルク読み込み.xlsx' , index=False, sheet_name='sheet1')
                    #csv ASIN別 詳細ページ売上・トラフィック
                    form_sku=request.POST.get('sku_text')

                    sku_list=form_sku.split(",")

                    df_list = pd.DataFrame()

                    # df_list = df_list.assign(SKU=sku_list)
                    df_list['SKU'] = sku_list

                    # df_list.to_excel(MEDIA_URL + 'SKUlist.xlsx' , index=False, sheet_name='sheet1')



                    #form skuをマージ
                    summary_df = pd.concat([up_filter1, df_list],sort=False)

                    # summary_df.to_excel(MEDIA_URL + 'merge1.xlsx' , index=False, sheet_name='sheet1')
                    #重複のみ抽出
                    summary_df = summary_df[summary_df.duplicated(keep=False,subset='SKU')]

                    # summary_df.to_excel(MEDIA_URL + '重複.xlsx' , index=False, sheet_name='sheet1')
                    #不備データの削除
                    summary_df = summary_df.dropna(subset=['キャンペーンID'])

                    # summary_df.to_excel(MEDIA_URL + '欠損データ削除.xlsx' , index=False, sheet_name='sheet1')
                    # summary_df = summary_df['ステータス']='保留中'

                    # テンプレートファイルの取得
                    temp_df= pd.read_excel(STATIC_URL + 'temp_1.xlsx', index_col=None)


                    temp_df['キャンペーン']=summary_df['キャンペーン']
                    temp_df['広告グループ']=summary_df['広告グループ']
                    temp_df['SKU']=summary_df['SKU']
                    temp_df['ステータス']='一時停止'

                    #マッチタイプ置き換え　不要になったら削除
                    temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('フレーズ','Phrase')
                    temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('完全一致','Exact')
                    temp_df['マッチタイプ']=temp_df.マッチタイプ.replace('除外完全一致','Negative Exact')

                    #出力ファイル名
                    dt_now = datetime.datetime.now()
                    d_str = dt_now.strftime('%Y%m%d%H%M%S')

                    #出力ファイル名
                    output_filename = 'Ex1_広告費削減施策' + d_str +'.xlsx'

                    temp_df.to_excel(MEDIA_URL + output_filename , index=False, sheet_name='sheet1')

                    wb = openpyxl.load_workbook(MEDIA_URL + output_filename)
                    response = HttpResponse(content_type='application/vnd.ms-excel')
                    # response['Content-Disposition'] = 'attachment; filename=' + output_filename
                    response['Content-Disposition'] = 'attachment; filename="{fn}"'.format(fn=urllib.parse.quote(output_filename))
                    wb.save(response)
                    return response

                except :
                    return redirect('/upload_error')
                    print("エラー")
                else:
                    print('例外は発生しませんでした。')



            else:
                print('p3 is_valid == False')
                #p2
                #データをSKUに設定
                try:
                    context = {
                        'form':UploadForm_Ex1_2(instance=obj),
                        'Setting': Setting_value_Ex1.objects.filter(UserID=user_name),
                        'page':7,
                        'title':'Ex1_広告費削減施策2P',
                        'user_ID':user_name,
                        'help_text':'「バルク操作ファイル６０日分」ファイル選択して下さい。',
                        'SKU':get('sku_text',None) ,
                    }

                except :
                    return redirect('/upload_error')
                    print("エラー")
                else:
                    print('例外は発生しませんでした。')



    #postがない場合（P1）
    else:
    #P1
        print("P1")
        try:

            context = {
                'form':UploadForm_Ex1_1(instance=obj),
                'Setting': Setting_value_Ex1.objects.filter(UserID=user_name),
                'page':6,
                'title':'Ex1_広告費削減施策P1',
                'user_ID':user_name,
                'help_text':'「ASIN別 詳細ページ売上・トラフィック」ファイルを選択して下さい。',

            }
        
        except :
            return redirect('/upload_error')
            print("エラー")
        else:
            print('例外は発生しませんでした。')

    return render(request , 'UploadEx1.html',context)


def upload_error(request):

    return render(request , 'upload_error.html')
