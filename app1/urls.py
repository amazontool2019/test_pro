from django.urls import path ,include
from .views import upload0,upload1,upload2,upload3,upload4,upload5,upload_error,uploadEx1
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views as auth_views
from . import views

app_name = 'app1'


urlpatterns = [

    # path('',ret_dataset),
    #path('/upload',UploadView.as_view()),
    # path('update',update,name='update'),

    # path('edit',edit,name='edit'),
    # path('data_update',download_form0.as_view()),
#   path('download',SingleUploadView.as_view()),
    # path('/download',SingleUploadView.as_view(),name='download'),

    # path('', views.IndexView.as_view(), name="home"),

    # path('<int:number>',IndexView.as_view()),
    #0新規広告
    path('',upload1,name='upload1'),
    path('upload0',upload0,name='upload0'),
    # path('',upload0,name='upload0'),
    #1_オート広告除外
    path('upload1',upload1,name='upload1'),
    #2キーワード仕入
    path('upload2',upload2,name='upload2'),

    #3入札額調整
    path('upload3',upload3,name='upload3'),
    #4マニュアル広告保留設定
    path('upload4',upload4,name='upload4'),
    #5完全一致移行
    path('upload5',upload5,name='upload5'),


    path('upload_error',views.upload_error,name='upload_error'),

    #Ex1広告費削減施策
    path('uploadEx1',uploadEx1,name='uploadEx1'),



]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
