from django import forms
from django.core.files.storage import default_storage
import openpyxl
from django.core.validators import FileExtensionValidator
from .models import Setting_value_0,Setting_value_1,Setting_value_2,Setting_value_3,Setting_value_4,Setting_value_5,Setting_value_Ex1
from django.core.validators import MinLengthValidator

class UploadForm_0(forms.ModelForm):
    class Meta:
        model = Setting_value_0
        fields=('cmp_price','bid','cmp_sep','def_keyword')

    #初期設定
    cmp_price = forms.IntegerField(label='キャンペーンの1日当たりの予算',widget=forms.NumberInput(attrs={
        'placeholder':'200'
    }))
    bid = forms.IntegerField(label='入札額',widget=forms.NumberInput(attrs={
        'placeholder':'16'
    }))
    cmp_sep = forms.CharField(max_length=1,label='キャンペーン名の2文字目の記号',widget=forms.TextInput(attrs={
        'placeholder':'_'
    }))

    def_keyword = forms.CharField(required=False,max_length=20,label='初期キーワード')


    file = forms.FileField(
    label='ファイル',
    # retuired=True
    help_text='<font color=red >新規広告作成用のファイルを選択して下さい。</font>',
    validators=[FileExtensionValidator(['xlsx'])]

    )

    def save(self, **kwargs):
    # def save(self):
        upload_file = self.cleaned_data['file']
        file_name = default_storage.save(upload_file.name, upload_file)

        super(UploadForm_0,self).save(**kwargs)

        return file_name

class UploadForm_1(forms.ModelForm):
    class Meta:
        model = Setting_value_1
        fields=('click_through',)

    click_through = forms.FloatField(label='クリックスルー率',widget=forms.NumberInput(attrs={
        'placeholder':'0.3'
    }))


    file = forms.FileField(
    label='ファイル',
    # retuired=True
    help_text='<font color=red >「スポンサープロダクト広告 検索ワード レポート60日分」ファイル選択して下さい。</font>',
    validators=[FileExtensionValidator(['xlsx'])]
    )


    def save(self, commit=True, *args, **kwargs):

        upload_file = self.cleaned_data['file']
        file_name = default_storage.save(upload_file.name, upload_file)

        m = super(UploadForm_1, self).save(commit=False, *args, **kwargs)

        m.click_through = m.click_through/100

        m.save()

        return file_name

class UploadForm_2(forms.ModelForm):
    class Meta:
        model = Setting_value_2
        fields=('click_through','click_add',)

    file = forms.FileField(
    label='ファイル',
    # retuired=True
    help_text='<font color=red >「スポンサープロダクト広告 検索ワード レポート60日分」ファイル選択して下さい。</font>',
    validators=[FileExtensionValidator(['xlsx'])]
    )


    #初期設定
    click_through = forms.FloatField(label='クリックスルー率',widget=forms.NumberInput(attrs={
        'placeholder':'0.3'
    }))
    click_add = forms.IntegerField(label='オート広告の平均クリック単価プラスの額',widget=forms.NumberInput(attrs={
        'placeholder':'3'
    }))


    def save(self, commit=True, *args, **kwargs):

        upload_file = self.cleaned_data['file']
        file_name = default_storage.save(upload_file.name, upload_file)

        m = super(UploadForm_2, self).save(commit=False, *args, **kwargs)

        m.click_through = m.click_through/100

        m.save()

        return file_name


class UploadForm_3(forms.ModelForm):
    class Meta:
        model = Setting_value_3
        fields=(
        'impression1',
        'impression2',
        'click_through1',
        'click_through2',
        'click_through3',
        'click_through4',
        )

        labels = {
        'impression1':    'インプレッション第1基準',
        'impression2':    'インプレッション第2基準',
        'click_through1':    'クリックスルー率第1基準',
        'click_through2':    'クリックスルー率第2基準',
        'click_through3':    'クリックスルー率第3基準',
        'click_through4':    'クリックスルー率第4基準',
        }

    file = forms.FileField(
    label='ファイル',
    # retuired=True
    help_text='<font color=red >「バルク操作ファイル７日分 」ファイル選択して下さい。</font>',
    validators=[FileExtensionValidator(['xlsx'])]
    )



    #初期設定
    impression1 = forms.IntegerField(label='インプレッション第1基準',widget=forms.NumberInput(attrs={
        'placeholder':'1000'
    }))
    impression2 = forms.IntegerField(label='インプレッション第2基準',widget=forms.NumberInput(attrs={
        'placeholder':'500'
    }))
    click_through1 = forms.FloatField(label='クリックスルー率第1基準',widget=forms.NumberInput(attrs={
        'placeholder':'0.5'
    }))
    click_through2 = forms.FloatField(label='クリックスルー率第2基準',widget=forms.NumberInput(attrs={
        'placeholder':'0.3'
    }))
    click_through3 = forms.FloatField(label='クリックスルー率第3基準',widget=forms.NumberInput(attrs={
        'placeholder':'0.2'
    }))
    click_through4 = forms.FloatField(label='クリックスルー率第4基準',widget=forms.NumberInput(attrs={
        'placeholder':'0.1'
    }))


    def save(self, commit=True, *args, **kwargs):

        upload_file = self.cleaned_data['file']
        file_name = default_storage.save(upload_file.name, upload_file)

        m = super(UploadForm_3, self).save(commit=False, *args, **kwargs)

        m.click_through1 = m.click_through1/100
        m.click_through2 = m.click_through2/100
        m.click_through3 = m.click_through3/100
        m.click_through4 = m.click_through4/100

        m.save()

        return file_name

class UploadForm_4(forms.ModelForm):
    class Meta:
        model = Setting_value_4
        fields=('click_through','impression','conversion')

    file = forms.FileField(
    label='ファイル',
    # retuired=True
    help_text='<font color=red >「バルク操作ファイル６０日分 」ファイル選択して下さい。</font>',
    validators=[FileExtensionValidator(['xlsx'])]
    )

    #初期設定
    click_through = forms.FloatField(label='クリックスルー率',widget=forms.NumberInput(attrs={
        'placeholder':'0.3'
    }))
    impression = forms.IntegerField(label='インプレッション',widget=forms.NumberInput(attrs={
        'placeholder':'500'
    }))
    conversion = forms.FloatField(label='コンバージョン率',widget=forms.NumberInput(attrs={
        'placeholder':'2.0'
    }))

    def save(self, commit=True, *args, **kwargs):

        upload_file = self.cleaned_data['file']
        file_name = default_storage.save(upload_file.name, upload_file)

        m = super(UploadForm_4, self).save(commit=False, *args, **kwargs)

        m.click_through = m.click_through/100
        m.conversion = m.conversion/100

        m.save()

        return file_name

class UploadForm_5(forms.ModelForm):
    class Meta:
        model = Setting_value_5
        fields=('click_through','impression','conversion','bid_add')


    file = forms.FileField(
    label='ファイル',
    # retuired=True
    help_text='<font color=red >「バルク操作ファイル６０日分」ファイル選択して下さい。</font>',
    validators=[FileExtensionValidator(['xlsx'])]
    )
    #初期設定
    click_through = forms.FloatField(label='クリックスルー率',widget=forms.NumberInput(attrs={
        'placeholder':'0.5'
    }))
    impression = forms.IntegerField(label='インプレッション',widget=forms.NumberInput(attrs={
        'placeholder':'2000'
    }))
    conversion = forms.FloatField(label='コンバージョン率',widget=forms.NumberInput(attrs={
        'placeholder':'2.0'
    }))
    bid_add = forms.IntegerField(label='プラスアルファ入札額',widget=forms.NumberInput(attrs={
        'placeholder':'3'
    }))

    def save(self, commit=True, *args, **kwargs):

        upload_file = self.cleaned_data['file']
        file_name = default_storage.save(upload_file.name, upload_file)

        m = super(UploadForm_5, self).save(commit=False, *args, **kwargs)

        m.click_through = m.click_through/100
        m.conversion = m.conversion/100

        m.save()

        return file_name

class UploadForm_Ex1(forms.ModelForm):
    class Meta:
        model = Setting_value_Ex1
        fields=('sales_lower_rate','impression','acos')


    sales_lower_rate = forms.FloatField(label='売上下位',widget=forms.NumberInput(attrs={
        'placeholder':'50.0'
    }))
    impression = forms.IntegerField(label='インプレッション',widget=forms.NumberInput(attrs={
        'placeholder':'500'
    }))

    acos = forms.FloatField(label='売上高に占める広告費の割合 （ACoS)',widget=forms.NumberInput(attrs={
        'placeholder':'50.0'
    }))


    def save(self, commit=True, *args, **kwargs):

        upload_file = self.cleaned_data['file']
        file_name = default_storage.save(upload_file.name, upload_file)

        m = super(UploadForm_Ex1, self).save(commit=False, *args, **kwargs)

        m.sales_lower_rate = m.sales_lower_rate/100
        m.acos = m.acos/100

        m.save()

        return file_name



class UploadForm_Ex1_1(UploadForm_Ex1):

    file = forms.FileField(
    label='ファイル1',
    # retuired=True
    help_text='<font color=red >「バルク操作ファイル６０日分」ファイル選択して下さい。</font>',

    validators=[FileExtensionValidator(['csv'])]

    )

class UploadForm_Ex1_2(UploadForm_Ex1):

    file = forms.FileField(
        label='ファイル2',
        # retuired=True
        help_text='<font color=red >「バルク操作ファイル６０日分」ファイル選択して下さい。</font>',

        validators=[FileExtensionValidator(['xlsx'])]
    )

    sales_lower_rate = forms.FloatField(label='売上下位',widget=forms.NumberInput(attrs={'readonly': 'readonly'}))
    impression = forms.IntegerField(label='インプレッション',widget=forms.NumberInput(attrs={'readonly': 'readonly'}))
    acos = forms.IntegerField(label='売上高に占める広告費の割合 （ACoS)',widget=forms.NumberInput(attrs={'readonly': 'readonly'}))
