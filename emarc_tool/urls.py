from django.contrib import admin
from django.urls import path ,include
# import app1.views as app1
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('app1.urls')),
    path('accounts/', include('allauth.urls')),

    # path('', include('allauth.urls')),
    # path('', include('allauth.urls')),
    # path('', TemplateView.as_view(template_name='home.html'), name='home'),#追加
    # path('accounts/login/', TemplateView.as_view(template_name = 'login.html'), name='login'),

]
