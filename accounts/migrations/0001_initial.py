# Generated by Django 3.0.5 on 2020-05-29 12:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='setting_value_0',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_name', models.CharField(max_length=10)),
                ('cmp_price', models.IntegerField(default=200)),
                ('bid', models.IntegerField(default=16)),
                ('cmp_sep', models.CharField(default='_', max_length=30)),
                ('def_keyword', models.CharField(blank=True, max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Sponser_Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Field1', models.CharField(max_length=200)),
                ('Field2', models.CharField(max_length=200)),
                ('Field3', models.CharField(max_length=200)),
                ('Field4', models.CharField(max_length=200)),
            ],
        ),
    ]
